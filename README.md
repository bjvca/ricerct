This repository collects and tracks all work related to a research project:

The Role of Information in Agricultural Technology Adoption: Experimental Evidence from Rice Farmers in Uganda
Bjorn Van Campenhout, Wilberforce Walukano and Piet Van Asten

Previous research identified information inefficiencies as a major constraint to sustainable crop intensification among rice farmers in Eastern Uganda. The fact that some farmers report not using certain inputs or techniques because they are not aware of them while others report they are aware of them but are not using them suggests information gaps at two levels. First, farmers may lack knowledge about the existence or use a particular input or technology. Second, a farmer may not be aware of the returns to using the technology. In this study we therefore try out two different information treatments at the individual level. In a first intervention, we show farmers the recommended practices and inputs in rice farming. In a second intervention, we point out the returns to investment in a series of simple simulations that also consider the longer run. The study uses a 2 by 2 factorial design with randomization over matched blocks of four farmers.

The entire project is under version control to increase transparency in research and to allow for easy replication. Note that due to privacy reasons, some files may be missing though.