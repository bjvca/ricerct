library(foreign)
sample <- read.csv("/home/bjvca/data/projects/PASIC/riceRCT/sampling/sample_rice_rnd.csv")
psec0 <- read.dta("/home/bjvca/data/projects/PASIC/data/rice/raw/version2/RSEC0.dta")[c("hhid","district", "sub_county", "parish", "lc1", "village", "ea", "head_name", "respondent_name", "gpsndeg", "gpsnmin", "gpsnsec", "gpsedeg","gpsemin","gpsesec","gpsalt", "hh3phone")]
sample_ID <- merge(sample,psec0, by="hhid")
write.csv(sample_ID,"/home/bjvca/data/projects/PASIC/riceRCT/sampling/sample_ID.csv")


#prepare for plotting using pymap
sample_ID$lat <-  sample_ID$gpsndeg + sample_ID$gpsnmin/60 + sample_ID$gpsnsec/3600
sample_ID$long <-  sample_ID$gpsedeg + sample_ID$gpsemin/60 + sample_ID$gpsesec/3600
sample_IDplot <- sample_ID[c("lat","long","treat","bloc")]
sample_IDplot <- sample_IDplot[complete.cases(sample_IDplot),]
write.table(sample_IDplot,"/home/bjvca/data/projects/PASIC/riceRCT/sampling/sample_GIS.csv", sep=",", row.names=FALSE, col.names=FALSE)
## call to external python program that uses "/home/bjvca/data/projects/PASIC/potseedRCT/sampling/sample_GIS.csv" as input
system("python /home/bjvca/data/projects/PASIC/riceRCT/sampling/maps.py")
system("cp '/home/bjvca/data/projects/PASIC/riceRCT/sampling/mymap.draw.html' '/home/bjvca/data/projects/PASIC/riceRCT/sampling/sample.html'")

### make a map for public use
sample_IDplot$lat <- jitter(sample_IDplot$lat,100)  
sample_IDplot$long <- jitter(sample_IDplot$long,100) 
write.table(sample_IDplot,"/home/bjvca/data/projects/PASIC/riceRCT/sampling/sample_GIS_pub.csv", sep=",", row.names=FALSE, col.names=FALSE)
## call to external python program that uses "/home/bjvca/data/projects/PASIC/potseedRCT/sampling/sample_GIS_pub.csv" as input
system("python /home/bjvca/data/projects/PASIC/riceRCT/sampling/maps.py")
system("cp '/home/bjvca/data/projects/PASIC/riceRCT/sampling/mymap.draw.html' '/home/bjvca/data/projects/PASIC/riceRCT/sampling/sample_pub.html'")

