#LyX 2.2 created this file. For more info see http://www.lyx.org/
\lyxformat 508
\begin_document
\begin_header
\save_transient_properties true
\origin unavailable
\textclass article
\use_default_options true
\maintain_unincluded_children false
\language english
\language_package default
\inputencoding auto
\fontencoding global
\font_roman "default" "default"
\font_sans "default" "default"
\font_typewriter "default" "default"
\font_math "auto" "auto"
\font_default_family default
\use_non_tex_fonts false
\font_sc false
\font_osf false
\font_sf_scale 100 100
\font_tt_scale 100 100
\graphics default
\default_output_format default
\output_sync 0
\bibtex_command default
\index_command default
\paperfontsize default
\use_hyperref false
\papersize default
\use_geometry false
\use_package amsmath 1
\use_package amssymb 1
\use_package cancel 1
\use_package esint 1
\use_package mathdots 1
\use_package mathtools 1
\use_package mhchem 1
\use_package stackrel 1
\use_package stmaryrd 1
\use_package undertilde 1
\cite_engine basic
\cite_engine_type default
\biblio_style plain
\use_bibtopic false
\use_indices false
\paperorientation portrait
\suppress_date false
\justification true
\use_refstyle 1
\index Index
\shortcut idx
\color #008000
\end_index
\secnumdepth 3
\tocdepth 3
\paragraph_separation indent
\paragraph_indentation default
\quotes_language english
\papercolumns 1
\papersides 1
\paperpagestyle default
\tracking_changes false
\output_changes false
\html_math_output 0
\html_css_as_file 0
\html_be_strict false
\end_header

\begin_body

\begin_layout Title
Intervention design
\end_layout

\begin_layout Part*
Introduction
\end_layout

\begin_layout Standard
This document describes the interventions for the study 
\begin_inset Quotes eld
\end_inset

The Role of Information in Agricultural Technology Adoption: Experimental
 Evidence from Rice Farmers in Uganda
\begin_inset Quotes erd
\end_inset

.
 The main hypotheses are that farmers do not engage in crop intensification
 (we focus on inorganic fertilizer and the use of proper practices related
 to timing and water management) because of information deficiencies.
 We make a distinction between information gaps at two levels.
 One information gap relates to existence and proper use of inputs or technologi
es.
 A second gap relates to profitability of inputs or technologies.
 We will test these hypotheses by a social experiment where we try to reduce
 these information gaps though small interventions in the form of educational
 agricultural extension videos.
 One video will focus on technical aspects related to crop intensification
 and describe how fertilizer can should be used, how water should be managed
 and how activities should be timed.
 A second video will focus on profitability by contrasting effects on yields
 to costs of the same inputs of activities shown in the first video.
 For instance, the video will take the farmer through a full cost-benefit
 calculation for fertilizer use.
 But it will also illustrate the expected loss in yields related to 
\begin_inset Quotes eld
\end_inset

late transplanting
\begin_inset Quotes erd
\end_inset


\begin_inset Foot
status collapsed

\begin_layout Plain Layout
As much as possible, the first video should avoid any referent to why an
 input or technology should be used.
 At the same time, the second video should avoid providing info on how to
 do things.
 For instance, the second video should refer to late transplanting, without
 specifying that late transplanting means after 3 weeks.
\end_layout

\end_inset

.
 We will also use a third video that is shown to all farmers as a placebo
 video.
 This video will be about proper storing and handling of rice.
 More information of the study is available at the 
\begin_inset CommandInset href
LatexCommand href
name "AEA RCT registry"
target "https://www.socialscienceregistry.org/trials/1312"

\end_inset

.
\end_layout

\begin_layout Part*
Interventions 
\end_layout

\begin_layout Section*
Intervention on Techniques
\end_layout

\begin_layout Standard
This intervention focuses on techniques and should be presented as such
 - down to earth, focusing on the how-to without mentioning the 
\begin_inset Quotes eld
\end_inset

why
\begin_inset Quotes erd
\end_inset


\end_layout

\begin_layout Itemize
It is important that you plan well ahead.
 
\end_layout

\begin_layout Itemize
Before sowing, prepare bunds in the rice field.
\end_layout

\begin_layout Itemize
bunds should be about 60 cm high and 50 cm width.
\end_layout

\begin_layout Itemize
plough the field, this is easier when the ground is moist
\end_layout

\begin_layout Itemize
after ploughing, submerge the field
\end_layout

\begin_layout Itemize
at this time, start the nursery, sowing 15 kg of clean seed per acre
\end_layout

\begin_layout Itemize
start paddling and leveling of the submerged field
\end_layout

\begin_layout Itemize
14 days after sowing, reduce water to at most 1 inch and makes sure all
 weeds are gone
\end_layout

\begin_layout Itemize
apply NPK (or DAP) at 25 kg per acre while transplanting
\end_layout

\begin_layout Itemize
transplant 2 seedlings per hill, 1.5 inches deep 20 cm from between each
 hill, preferably in rows.
\end_layout

\begin_layout Itemize
then keep field moist for 14 days
\end_layout

\begin_layout Itemize
add water up to 2 inches and remove weeds
\end_layout

\begin_layout Itemize
apply urea 25kg/acre
\end_layout

\begin_layout Itemize
keep the water *in* the field for 14 days - if water levels drop due to
 eg evaporation, add water to maintain optimal levels of 2 inches
\end_layout

\begin_layout Itemize
five weeks after apply second time of urea after weeding, again 25 kg per
 acre.
 Again, keep water in the field for 14 days.
 
\end_layout

\begin_layout Itemize
increase water levels up to 3 inches, this depth should be maintained up
 to maturing.
 
\end_layout

\begin_layout Itemize
One week before harvest, gradually reduce water to zero by harvesting day.
\end_layout

\begin_layout Section*
Intervention on Profitability
\end_layout

\begin_layout Standard
This intervention should be much less of a how-to, but more of a video that
 points out facts relevant for cost benefit analysis, such as effect of
 inputs and methods on yields, making farmers aware that investing now has
 payoffs in the future (both monetary as in terms of time) and encourages
 farmers to take a long run perspective when making decisions.
 We need to make sure farmers do not get to see images that provide clues
 on how to do things.
 We want the video to focus on the folowing 3 skills:
\end_layout

\begin_layout Enumerate
yield increase that can be expected from timelines, water management and
 nutrient management:
\end_layout

\begin_deeper
\begin_layout Itemize
Timeliness: transplanting in time increases yields by 3.5 bags per acre.
\end_layout

\begin_layout Itemize
Proper water management reduces yields by 3 bags per acre
\end_layout

\begin_layout Itemize
fertilizer use on average increases yields by 3.6 bags per acre
\end_layout

\end_deeper
\begin_layout Enumerate
explain idea of intertemporal investment - visualize how by investing some
 money now, you can get more money in the future.
 This is also the case for time: investing some labour now, will mean one
 has more time later.
 We could for instance visualize this by showing one farmer who spends 2000
 UGX on local booze and wastes his time drinking it with friends, while
 another farmer spends 2000 UGX on fertilizer and his time applying the
 fertilizer while transplanting.
 We can then fast forward to the future and show the first farmer receiving
 little money (and making himself not popular as he needs to beg for booze
 with his friends), while the second farmer gets more money and now enjoys
 his time with friends who he can buy a proper beer (Eagle Lager).
\end_layout

\begin_layout Enumerate
taking a longer run view (explaining sunk costs and recurrent revenues):
\end_layout

\begin_deeper
\begin_layout Itemize
mention that proper water management (without becoming too specific and
 show how to manage water) has long run effects on soil since it conserves
 top soil.
\end_layout

\begin_layout Itemize
illustrate how profits from fertilizer can be reinvested and generate progressiv
ely bigger stream of income (start small)
\end_layout

\begin_layout Itemize
show planning and managing the farm as a commercial entity transforms lives,
 that allocating time now will provide you time when you are old.
\end_layout

\end_deeper
\begin_layout Section*
Intervention on post harvest handling and marketing
\end_layout

\begin_layout Standard
This is the control intervention that mixes techniques and issues related
 to profitability.
 As such, it can contain parts that are show 
\emph on
how
\emph default
 to do things but also explain 
\emph on
why
\emph default
 it is important, as well as the ultimate goal of increasing quality of
 end product and getting a higher price for rice.
 
\end_layout

\begin_layout Itemize
paddy must be put on a tarpaulin - doing this correctly has the potential
 to increase price from on average 1700 UGX per kg to 1800 per kg (6pct
 increase).
 Maybe this is best expressed per bag, eg.
 it has the potential of increasing the price you get for a bag of your
 rice from 170,000 to 180,000.
 
\end_layout

\begin_layout Itemize
paddy should be spread out at thickness of about 5 cm and should be shuffled
 every 30 minutes to allow equal exposure to sun - doing this has the potential
 to increase price from on average 1700 UGX per kg to 1870 UGX per kg (10
 pct increase).
\end_layout

\begin_layout Itemize
drying paddy should not be left in direct sunlight for more than 3 hours,
 dry like this for 3 to 4 days, dry slowly to avoid broken rice - doing
 this correctly has the potential to increase price from on average 1700
 UGX per kg to 1785 per kg (5pct increase).
\end_layout

\begin_layout Itemize
don't sell rice immediately after harvest, but wait 3 months.
 This has the potential to increase the price you get from your rice from
 1700 per kg to 1800 per kg.
 If you are in urgent need for money, sell only part and make sure you know
 the going price in the market.
\end_layout

\begin_layout Part*
Control questions
\end_layout

\begin_layout Standard
After the interventions, we will ask a very short series of questions.
 These questions are simply to test if the information that was given in
 the videos was understood by the farmers.
 We may use the results of these questions in our final regressions to investiga
te impact pathways.
\end_layout

\begin_layout Standard
The first three questions are thought experiments.
 If the farmer answers with ...between x and y bags, write down the mean.
 
\end_layout

\begin_layout Itemize
Fertilizer:
\end_layout

\begin_deeper
\begin_layout Itemize
Suppose you are farming one acre of rice.
 Now suppose that you are not using fertilizer at all.
 How many bags would you be able to harvest from one acre? 
\end_layout

\begin_layout Itemize
Now suppose you are farming one acre of rice, except that this time, you
 would be using fertilizer according to recommendations.
 How many bags would you be able to harvest from one acre? 
\end_layout

\end_deeper
\begin_layout Itemize
Time management:
\end_layout

\begin_deeper
\begin_layout Itemize
Suppose you are farming one acre of rice.
 Now suppose that you are planning well ahead, and do everything at recommended
 moments in time.
 With perfect time management, how many bags would you be able to harvest
 from one acre? 
\end_layout

\begin_layout Itemize
Now suppose you are farming one acre of rice, except that this time, you
 are not planning well ahead, and are late with everything.
 With poor time management, how many bags would you be able to harvest from
 one acre? 
\end_layout

\end_deeper
\begin_layout Itemize
Water management:
\end_layout

\begin_deeper
\begin_layout Itemize
Suppose you are farming one acre of rice.
 Now suppose that you are able to manage water levels according to recommendatio
ns.
 With perfect water management, how many bags would you be able to harvest
 from one acre? 
\end_layout

\begin_layout Itemize
Now suppose you are farming one acre of rice, except that this time, for
 some reason, you are unable to manage water levels according to recommendations.
 With poor water management, how many bags would you be able to harvest
 from one acre? 
\end_layout

\end_deeper
\begin_layout Itemize
Seeds: 
\end_layout

\begin_deeper
\begin_layout Itemize
Suppose you are farming one acre of rice.
 Now suppose that you are able get improved seed.
 With good quality seed, how many bags would you be able to harvest from
 one acre? 
\end_layout

\begin_layout Itemize
Now suppose you are farming one acre of rice, except that this time, you
 are using farmer saved seed.
 With poor quality seed, how many bags would you be able to harvest from
 one acre? 
\end_layout

\end_deeper
\begin_layout Itemize
When is the best time to transplant? 
\end_layout

\begin_deeper
\begin_layout Itemize
after 14 days for optimal tillering
\end_layout

\begin_layout Itemize
after 25 days to avoid broke rice kernels
\end_layout

\begin_layout Itemize
after 35 days to avoid premature flowering
\end_layout

\end_deeper
\begin_layout Itemize
How often should you apply UREA to rice:
\end_layout

\begin_deeper
\begin_layout Itemize
once, 2 weeks after transplanting
\end_layout

\begin_layout Itemize
twice, 2 weeks after transplanting and 7 weeks after transplanting
\end_layout

\begin_layout Itemize
3 times, during transplanting, 2 weeks after transplanting and 7 weeks after
 transplanting
\end_layout

\end_deeper
\begin_layout Itemize
After Urea application:
\end_layout

\begin_deeper
\begin_layout Itemize
water should be reduced gradually to increase infiltration of fertilizer
\end_layout

\begin_layout Itemize
water should be drained to allow fertilizer to reach the roots of the rice
 plants
\end_layout

\begin_layout Itemize
water should be kept in the field to avoid wastage of fertilizer
\end_layout

\end_deeper
\begin_layout Itemize
What should be done with straws after harvest
\end_layout

\begin_deeper
\begin_layout Itemize
spread straws in the field to decompose
\end_layout

\begin_layout Itemize
use straws for bund construction
\end_layout

\begin_layout Itemize
burn straws to avoid contaminating the field with diseased crop residue.
\end_layout

\end_deeper
\begin_layout Itemize
After harvesting, you should:
\end_layout

\begin_deeper
\begin_layout Itemize
Keep paddy on heaps for one day to cool down before threshing
\end_layout

\begin_layout Itemize
thresh on the same day to avoid brown coloration
\end_layout

\begin_layout Itemize
keep paddy on a heap for at least two days to make threshing easier.
\end_layout

\end_deeper
\end_body
\end_document
