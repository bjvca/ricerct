### AWS script

wget https://www.dropbox.com/s/v93bzipctadm03w/dta.csv?dl=0

sudo apt-get install r-base
sudo R
install.packages(doParallel) 


###from here on in R
rm(list=ls())
library(doParallel)

cl <- makeCluster(detectCores())
registerDoParallel(cl)

spillSim <- function(y_z, z, bloc, mat,nr_repl = 1000, beta1 = c(-10,10,1), beta2 = c(-1,1,.1), alpha = .05) {
### function to perform spillover analysis following Bowers, Fredrickson and Panagopoulos (2013)
###  spillSim(Y, assign,true_adjmat, 1000,beta1 = c(1,3,.5), beta2 = c(-1,1,.5) )

beta1_lower <- beta1[1]
beta1_upper <- beta1[2]
beta1_step <- beta1[3]

beta2_lower <- beta2[1]
beta2_upper <- beta2[2]
beta2_step <- beta2[3]

#mat <- mat < .002
diag(mat) <- 0
RSS_mod <- deviance(lm(y_z ~ z + (mat %*% z)))

### init results matrix
coll_mat <- matrix(NA,length(seq( beta1_lower,beta1_upper, by = beta1_step)), length(seq( beta2_lower,beta2_upper, by = beta2_step)) )
ind_j <- 1
for (beta_1 in seq( beta1_lower,beta1_upper, by = beta1_step)) {
	ind_i <- 1
	for (beta_2 in seq( beta2_lower,beta2_upper, by = beta2_step))  {
		
		print(beta_1)
		print(beta_2)
		### compute uniformity trial for beta_1 and beta_2
		y_0 <- y_z - beta_1* z - beta_2 * (mat %*% z)
		sum <- 0
		oper <- foreach (repl = 1:nr_repl,.combine=cbind) %dopar% {
		w <- data.frame(cbind(as.vector(sapply(1:63,function(x) sample(c(1,1,0,0)))),rep(1:63, each=4),rep(1:4, times=63)))
		names(w) <- c("w","bloc","id")
		dta_int <- data.frame(bloc)
		dta_int$one <- 1
		names(dta_int) <- c("bloc","one")
		dta_int <- data.frame(cbind(dta_int$bloc,ave(dta_int$one,dta_int$bloc, FUN = seq_along)), y_0)
		names(dta_int) <- c("bloc","id", "y_0")
		dta_int <- merge(dta_int,w, by =c("bloc","id"))



			#w <- complete_ra(length(z),sum(z))
			dta_int$y_w <- dta_int$y_0 + beta_1 * dta_int$w + beta_2 * (mat %*% dta_int$w)
			return(as.numeric(deviance(lm(dta_int$y_w ~ dta_int$w + (mat %*% dta_int$w) )) < RSS_mod ))
			
		}
		coll_mat[ind_j, ind_i] <- sum(as.vector(oper))/nr_repl
		ind_i <- ind_i + 1

	}
	ind_j <- ind_j + 1
}
rownames(coll_mat) <- seq( beta1_lower,beta1_upper, by = beta1_step)
colnames(coll_mat) <- seq( beta2_lower,beta2_upper, by = beta2_step)
beta_1 <- rownames(coll_mat)[which(coll_mat == max(coll_mat), arr.ind = TRUE)[1]]
beta_2 <- colnames(coll_mat)[which(coll_mat == max(coll_mat), arr.ind = TRUE)[2]]

#use this to get the confidence interval for beta_1
#beta_1_dist <- coll_mat[,which(coll_mat == max(coll_mat), arr.ind = TRUE)[2]]
#beta_1_conf <- c(min(as.numeric(names(beta_1_dist[beta_1_dist > alpha/2]))) , max(as.numeric(names(beta_1_dist[beta_1_dist > alpha/2]))))

#use this to get the confidence interval for beta_2
#beta_2_dist <- coll_mat[which(coll_mat == max(coll_mat), arr.ind = TRUE)[1],]
#beta_2_conf <- c(min(as.numeric(names(beta_2_dist[beta_2_dist > alpha/2]))) , max(as.numeric(names(beta_2_dist[beta_2_dist > alpha/2]))))

#res <- list(coll_mat, beta_1, beta_2, beta_1_conf, beta_2_conf)
res <- list(coll_mat, beta_1, beta_2)

return(res)
}

dta2 <- read.csv("dta.csv")
dta2 <- read.csv("area.csv")
dta2 <- read.csv("rice_share.csv")
dta2 <- read.csv("rice_yield.csv")
dta2 <- read.csv("hh_trans.csv")
dta2 <- read.csv("hh_h2o.csv")
dta2 <- read.csv("hh_nurse.csv")
dta2 <- read.csv("hh_nurse.csv")
dta2 <- read.csv("hh_rowplant.csv")

