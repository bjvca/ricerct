sample <- read.csv("/home/bjvca/data/projects/PASIC/riceRCT/sampling/sample_ID.csv")


### correcting village (ea) names
sample$ea <- as.character(sample$ea)



sample$ea[sample$ea == "BUGOYOZA A"] <- "BUGOYOZI A C"
sample$ea[sample$ea == "BUGOYOZI A"] <- "BUGOYOZI A C"
sample$ea[sample$ea == "BUGOYOZI A 'C'"] <- "BUGOYOZI A C"
sample$ea[sample$ea == "BUGOYOZI A 'C'"] <- "BUGOYOZI A C"
sample$ea[sample$ea == "BUGOYOZI A'C'"] <- "BUGOYOZI A C"
sample$ea[sample$ea == "BUGOYOZI AC"] <- "BUGOYOZI A C"
sample$ea[sample$ea == "IMULI 'A'"] <- "IMULI A"
sample$ea[sample$ea == "KAWOLOGAMA"] <- "KAWOLOGAMA B"
sample$ea[sample$ea == "KAWOLOGOMA B"] <- "KAWOLOGAMA B"
sample$ea[sample$ea == "BUKONDE B LC1"] <- "BUKONDE B"
sample$ea[sample$ea == "NABIYUNYU 'B'"] <- "NABIYUNYU B"
sample$ea[sample$ea == "NABYUNYU B"] <- "NABIYUNYU B"
sample$ea[sample$ea == "NAMAYEMBA A'A'"] <- "NAMAYEMBA CENTRAL A"
sample$ea[sample$ea == "WANENGA"] <- "WANENGA B"
sample$ea[sample$ea == "WANENGA.B"] <- "WANENGA B"
sample$ea[sample$ea == "KANGHANYI"] <- "KANGAHANYI"
sample$ea[sample$ea == "NAHAMYA B LC 1"] <- "NAHAMYA B"
sample$ea[sample$ea == "NAHAMYA 'BLC1'"] <- "NAHAMYA B"
sample$ea[sample$ea == "NAMAMYA B LC1"] <- "NAHAMYA B"
sample$ea[sample$ea == "NAHAMYA B LC1"] <- "NAHAMYA B"
sample$ea[sample$ea == "NAKABALE LC 1 A"] <- "NAKABALE"
sample$ea[sample$ea == "NAKABALE LC I A"] <- "NAKABALE"
sample$ea[sample$ea == "NAKABALE LC1 A"] <- "NAKABALE"
sample$ea[sample$ea == "ASINGE (A)"] <- "ASINGE A"
sample$ea[sample$ea == "BUNGANGA A"] <- "BUNGANGA"
sample$ea[sample$ea == "BUGANGA"] <- "BUNGANGA"
sample$ea[sample$ea == "IYORIANG B"] <- "IYORIANG B A"
sample$ea[sample$ea == "IYORIANG'B'"] <- "IYORIANG B A"
sample$ea[sample$ea == "IYORIANG B'A'"] <- "IYORIANG B A"
sample$ea[sample$ea == "NAMBOGO A B"] <- "NAMBOGO A"
sample$ea[sample$ea == "NAMBOGO A 'B'"] <- "NAMBOGO A"
sample$ea[sample$ea == "NAMWANGA B"] <- "NAMWANGA"
sample$ea[sample$ea == "NAMWANGA B A"] <- "NAMWANGA"
sample$ea[sample$ea == "POYEM"] <- "POYEM B"
sample$ea[sample$ea == "POYEMB"] <- "POYEM B"
sample$ea[sample$ea == "PUYEM B"] <- "POYEM B"
sample$ea[sample$ea == "SESEME MUKEMERI LC1"] <- "SESEME MUKEMERI"
sample$ea[sample$ea == "SESEME MUKUMERI LC1"] <- "SESEME MUKEMERI"
sample$ea[sample$ea == "SESEME MUKUMERI"] <- "SESEME MUKEMERI"
sample$ea[sample$ea == "WANENGA .B"] <- "WANENGA B"

sample$ea <- factor(sample$ea)

tab <- table(sample$district,sample$ea)
### villages in Bugiri
print(rownames(tab)[1])
colnames(tab)[tab[1,] >0]
### villages in Butaleja
print(rownames(tab)[2])
colnames(tab)[tab[2,] >0]
### villages in Tororo
print(rownames(tab)[3])
colnames(tab)[tab[3,] >0]

tab2 <- table(sample$ea,sample$hhid)
for (i in 1:dim(tab2)[1]) {
print(rownames(tab2)[i])
print(colnames(tab2)[tab2[i,] >0])
}

control <- subset(sample,sample$treat == "Ctrl")

### correcting village (ea) names
control$ea <- as.character(control$ea)

control$ea[control$ea == "BUGARA A"] <- "BUGARA"
control$ea[control$ea == "RWABARERA"] <- "RWABAREERA"
control$ea[control$ea == "BWAYO"] <- "BWAYU"
control$ea[control$ea == "GAHEMBE A"] <- "GAHEMBE"
control$ea[control$ea == "KATAGIRAMAIZI A"] <- "KATAGIRAMAIZI"
control$ea[control$ea == "KATAGIRAMIZI A"] <- "KATAGIRAMAIZI"
control$ea[control$ea == "KATAGIRAMIZI"] <- "KATAGIRAMAIZI"
control$ea[control$ea == "KIBURARA A"] <- "KIBURARA"
control$ea[control$ea == "KIDAKAMA B"] <- "KIDAKAMA"
control$ea[control$ea == "KANYABITARA LCI A"] <- "KANYABITARA"
control$ea[control$ea == "KYAJURA B"] <- "KYAJURA"
control$ea[control$ea == "KYANJURA B"] <- "KYAJURA"
control$ea[control$ea == "KYAZURA"] <- "KYAJURA"
control$ea[control$ea == "KYAZURA"] <- "KYAJURA"
control$ea[control$ea == "OMURUSHASHA B"] <- "OMURUSHASHA"
control$ea[control$ea == "MURUSHASHA"] <- "OMURUSHASHA"
control$ea[control$ea == "RUSHASHA"] <- "OMURUSHASHA"
control$ea[control$ea == "RUHAGO A"] <- "RUHAGO"
control$ea[control$ea == "BUTENGO C"] <- "BUTENGO"
control$ea[control$ea == "MULORA"] <- "MURORA"
control$ea[control$ea == "MURORA B"] <- "MURORA"
control$ea[control$ea == "RUGESHI A"] <- "RUGESHI"
control$ea[control$ea == "KIGATA A"] <- "KIGATA"
control$ea[control$ea == "RUGESHI A"] <- "RUGESHI"
control$ea[control$ea == "BUTUGA B"] <- "BUTUGA"
control$ea[control$ea == "MURUSHASHA B"] <- "MURUSHASHA"
control$ea[control$ea == "GIKORO B"] <- "GIKORO"

control$ea <- factor(control$ea)

tab <- table(control$district,control$ea)
### villages in Kabale
print(rownames(tab)[1])
colnames(tab)[tab[1,] >0]
### villages in kanungu
print(rownames(tab)[2])
colnames(tab)[tab[2,] >0]
### villages in kisoro
print(rownames(tab)[3])
colnames(tab)[tab[3,] >0]

tab2 <- table(control$ea,control$hhid)
for (i in 1:dim(tab2)[1]) {
print(rownames(tab2)[i])
print(colnames(tab2)[tab2[i,] >0])
}
